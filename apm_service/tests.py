import asyncio
import storage as s

loop = asyncio.get_event_loop()

stor = s.Storage("postgres://dbuser:@localhost/apm")

loop.run_until_complete(stor.connect())


async def get_taks(pk):
    task = await stor.get_task(pk)
    print(task)


async def set_result(pk, res):
    task = await stor.task_set_result(pk, res)
    print(task)


async def get_parsers():
    task = await stor.get_parsers()
    print(task)


loop.run_until_complete(get_taks('785548d2-6756-4bb9-b25d-e4e789559329'))

loop.run_until_complete(get_taks('785548d2-6756-4bb9-b25d-e4e789559333'))

loop.run_until_complete(set_result('785548d2-6756-4bb9-b25d-e4e789559329', {'foo': 'bar'}))

loop.run_until_complete(get_parsers())
