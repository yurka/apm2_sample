import asyncio
import logging

from apm_service.aparser_client import AParser


logging.basicConfig(
    format='%(asctime)-15s %(levelname)-8s %(name)s: %(message)s',
    level=logging.DEBUG
)


api_url = 'http://a-parser-node1.wstat.wiseweb.co:9095/API'
pwd = '46V2elhr74A8psA'


loop = asyncio.get_event_loop()


client = AParser(api_url, pwd)


def do(coro):
    res = loop.run_until_complete(coro)
    return res


print(do(client.info()))
