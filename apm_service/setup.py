from setuptools import setup


version = '0.1.0'


setup(
    name='apm-service',
    version=version,
    description='AParser Manager Service',
    packages=['apm_service'],
    zip_safe=False,
    platforms='any',
    install_requires=[
        'aiohttp',
        'aiohttp-swagger',
        'aiopg',
        'sqlalchemy',
        'configargparse',
        'funcy',
        'colander',
    ],
    include_package_data=True,
    entry_points={
        'console_scripts': [
            'apm-service = apm_service.main:run',
        ]
    }
)
