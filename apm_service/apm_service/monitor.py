import asyncio
import logging

logger = logging.getLogger(__name__)


class Monitor:

    def __init__(self, service, period=5):
        self.service = service
        self.period = period

    async def run(self):
        while True:
            await asyncio.sleep(self.period)
            logger.debug("monitor tick")
