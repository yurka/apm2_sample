import json
import logging

from aiohttp import web
from aiohttp_swagger import setup_swagger

# from apm_service.tasks import validate_task_input
from apm_service.handlers import registry
from apm_service.impala import ImpalaService

logger = logging.getLogger(__name__)


async def add_task(request):
    '''
    ---
    description: Add new parser task
    tags:
    - Tasks
    produces:
    - application/json
    parameters:
    - in: body
      name: task
      description: Task params.
      schema:
        type: object
        required:
          - name
          - params
        properties:
          name:
            type: string
            description: Task name
          params:
            type: object
            description: Task params
          callback:
            type: string
            description: callback URL
    - in: header
      name: AuthToken
      type: string
      required: true
    responses:
        '200':
            description: successful operation. Return added task ID
        '405':
            description: invalid HTTP Method
    '''
    try:
        task = await request.json()
    except json.JSONDecodeError:
        return web.json_response({"error": "Invalid JSON in request"}, status=400)

    logger.debug("API task received %s", task)
    err = registry.validate_task(task)
    if err:
        return web.json_response(err, status=400)
    task_id = await request.app.storage.put_task(task)
    resp = {"task_id": task_id}
    return web.json_response(resp)


async def get_task_status(request):
    '''
    ---
    description: Get task status
    tags:
    - Tasks
    produces:
    - application/json
    parameters:
    - in: path
      name: task_id
      type: string
      description: Task ID.
    - in: header
      name: AuthToken
      type: string
      required: true
    responses:
        '200':
            description: successful operation. Return task status
        '404':
            description: No task found
        '405':
            description: invalid HTTP Method
    '''
    task_id = request.match_info['task_id']
    task = await request.app.storage.get_task(task_id)
    if task is None:
        return web.json_response(status=404)
    resp = {'status': task['status'], 'parser_status': task['parser_status']}
    return web.json_response(resp)


async def get_task_result(request):
    '''
    ---
    description: Get task result
    tags:
    - Tasks
    produces:
    - application/json
    parameters:
    - in: path
      name: task_id
      type: string
      description: Task ID.
    - in: header
      name: AuthToken
      type: string
      required: true
    responses:
        '200':
            description: successful operation. Return task result
        '404':
            description: No task found
        '405':
            description: invalid HTTP Method
    '''
    task_id = request.match_info['task_id']
    task = await request.app.storage.get_task(task_id)
    if task is None:
        return web.json_response(status=404)
    # logger.debug("API task received %s", task)
    resp = {"result": task['result']}
    return web.json_response(resp)


async def get_task_result_file(request):
    '''
    ---
    description: Get task result file url
    tags:
    - Tasks
    produces:
    - application/json
    parameters:
    - in: path
      name: task_id
      type: string
      description: Task ID.
    - in: header
      name: AuthToken
      type: string
      required: true
    responses:
        '200':
            description: successful operation. Return task result file url
        '404':
            description: No task found
        '405':
            description: invalid HTTP Method
    '''
    task_id = request.match_info['task_id']
    task = await request.app.storage.get_task(task_id)
    if task is None:
        return web.json_response(status=404)
    # logger.debug("API task received %s", task)
    resp = await request.app.manager.get_task_result_file(task)
    return web.json_response(resp)


async def get_task_parser_state(request):
    '''
    ---
    description: Get task raw state from parser
    tags:
    - Tasks
    produces:
    - application/json
    parameters:
    - in: path
      name: task_id
      type: string
      description: Task ID.
    - in: header
      name: AuthToken
      type: string
      required: true
    responses:
        '200':
            description: successful operation. Return task state from parser
        '404':
            description: No task found
        '405':
            description: invalid HTTP Method
    '''
    task_id = request.match_info['task_id']
    task = await request.app.storage.get_task(task_id)
    if task is None:
        return web.json_response(status=404)
    # logger.debug("API task received %s", task)
    resp = await request.app.manager.get_task_state(task)
    return web.json_response(resp)


async def bulk_request(request):
    '''
    ---
    description: make bulk request
    tags:
    - Tasks
    produces:
    - application/json
    parameters:
    - in: body
      name: request
      description: Bulk Request params.
      schema:
        type: object
        required:
          - name
          - params
        properties:
          name:
            type: string
            description: Bulk Request name
          params:
            type: object
            description: Bulk Request parameters
    - in: header
      name: AuthToken
      type: string
      required: true
    '''
    try:
        bulk_request = await request.json()
    except json.JSONDecodeError:
        return web.json_response({"error": "Invalid JSON in request"}, status=400)
    logger.debug("API bulk request received %s", bulk_request)
    err = registry.validate_bulk_request(bulk_request)
    if err:
        return web.json_response(err, status=400)
    resp = await request.app.manager.bulk_request(bulk_request)
    return web.json_response(resp)


async def get_parser_info(request):
    '''
    ---
    description: Get parser info - parser api "info" action
    tags:
    - Parsers
    produces:
    - application/json
    parameters:
    - in: path
      name: parser_id
      type: string
      description: Parser ID from DB.
    - in: header
      name: AuthToken
      type: string
      required: true
    responses:
        '200':
            description: successful operation. Return result of "info" parser API call
        '404':
            description: No parser found
        '405':
            description: invalid HTTP Method
    '''
    parser_id = int(request.match_info['parser_id'])
    info = await request.app.manager.get_parser_info(parser_id)
    if info is None:
        return web.json_response(status=404)
    return web.json_response(info)


async def get_parser_tasks_list(request):
    '''
    ---
    description: Get parser tasks list - parser api "getTasksList" action
    tags:
    - Parsers
    produces:
    - application/json
    parameters:
    - in: path
      name: parser_id
      type: string
      description: Parser ID from DB.
    - in: header
      name: AuthToken
      type: string
      required: true
    responses:
        '200':
            description: successful operation. Return result of "getTasksList" parser API call
        '404':
            description: No parser found
        '405':
            description: invalid HTTP Method
    '''
    parser_id = int(request.match_info['parser_id'])
    info = await request.app.manager.get_parser_tasks_list(parser_id)
    if info is None:
        return web.json_response(status=404)
    return web.json_response(info)


async def loopback(request):
    '''
    ---
    description: Test task callback
    tags:
    - Tasks
    produces:
    - application/json
    parameters:
    - in: query
      name: status
      type: string
      description: status to return.
    '''
    status_str = request.query.get("status", "200")
    try:
        status = int(status_str)
    except ValueError:
        status = 200
    data = await request.text()
    logger.info("Loopback data: %s,status %s", data, status)
    return web.json_response({}, status=status)


async def get_help(request):
    '''
    ---
    description: Get description of all tasks/bulk request handlers
    tags:
    - Tasks
    produces:
    - application/json
    '''
    res = registry.describe()
    print(res)
    return web.json_response(res)


async def keywords_get_weights(request):
    '''
    ---
    description: Get keywords weights from impala table
    tags:
    - Impala
    produces:
    - application/json
    parameters:
    - in: body
      name: params
      description: Parameters
      required: true
      schema:
          type: object
          required:
            - table
            - keywords
          properties:
            keywords:
              type: array
              items:
                type: string
                description: Keywords
            exclude:
              type: array
              items:
                type: string
                description: Keywords to exclude
            table:
              type: string
              description: Table name
    - in: header
      name: AuthToken
      type: string
      required: true
    '''

    impala = ImpalaService()
    try:
        req = await request.json()
    except json.JSONDecodeError:
        return web.json_response({"error": "Invalid JSON in request"}, status=400)
    try:
        res = await request.app.loop.run_in_executor(
            None, impala.get_keywords_weights, req['table'], req.get('keywords'), req.get('exclude'))
        return web.json_response(res)
    except Exception as e:
        return web.json_response({"error": str(e)}, status=500)


async def keywords_get_clusters(request):
    '''
    ---
    description: Get keywords clusters from impala table
    tags:
    - Impala
    produces:
    - application/json
    parameters:
    - in: body
      name: params
      description: Parameters
      required: true
      schema:
          type: object
          required:
            - table
            - keywords
          properties:
            keywords:
              type: array
              items:
                type: string
                description: Keywords to match
            exclude:
              type: array
              items:
                type: string
                description: Keywords to exclude
            table:
              type: string
              description: Table name
    - in: header
      name: AuthToken
      type: string
      required: true
    '''

    impala = ImpalaService()
    try:
        req = await request.json()
    except json.JSONDecodeError:
        return web.json_response({"error": "Invalid JSON in request"}, status=400)
    try:
        res = await request.app.loop.run_in_executor(
            None, impala.get_keywords_clusters, req['table'],
            req.get('keywords'), req.get('exclude'), req.get('limit'),
            req.get('offset'))
        return web.json_response(res)
    except Exception as e:
        return web.json_response({"error": str(e)}, status=500)


async def keywords_get_category_clusters(request):
    '''
    ---
    description: Get keywords clusters for category from impala table
    tags:
    - Impala
    produces:
    - application/json
    parameters:
    - in: body
      name: params
      description: category
      required: true
      schema:
          type: object
          required:
            - table
            - category
          properties:
            category:
              type: string
              description: Category name
            table:
              type: string
              description: Table name
            cluster:
              type: number
              description: Cluster number - 3,4,5
    - in: header
      name: AuthToken
      type: string
      required: true
    '''

    impala = ImpalaService()
    try:
        req = await request.json()
        if req.get('cluster') not in {3,4,5}:
            return web.json_response({"error": "cluser number should be one of [3,4,5]"}, status=400)

    except json.JSONDecodeError:
        return web.json_response({"error": "Invalid JSON in request"}, status=400)
    try:
        res = await request.app.loop.run_in_executor(
            None, impala.get_category_clusters, req['table'], req['category'], req['cluster'])
        return web.json_response(res)
    except Exception as e:
        return web.json_response({"error": str(e)}, status=500)


async def auth_middleware(app, handler):
    async def middleware_handler(request):
        token = request.headers.get('authtoken')
        if app.service.options.authtoken != 'NOAUTH' and token != app.service.options.authtoken:
            raise web.HTTPUnauthorized()
        return await handler(request)
    return middleware_handler


app = web.Application(middlewares=[auth_middleware])

app.router.add_route('POST', '/tasks', add_task)
app.router.add_route('GET', '/tasks/{task_id}/status', get_task_status)
app.router.add_route('GET', '/tasks/{task_id}/result', get_task_result)
app.router.add_route('GET', '/tasks/{task_id}/parser_state', get_task_parser_state)
app.router.add_route('GET', '/tasks/{task_id}/result_file', get_task_result_file)
app.router.add_route('POST', '/bulk_request', bulk_request)

app.router.add_route('GET', '/parsers/{parser_id}/info', get_parser_info)
app.router.add_route('GET', '/parsers/{parser_id}/tasks', get_parser_tasks_list)


app.router.add_route('POST', '/keywords/get_weights/', keywords_get_weights)
app.router.add_route('POST', '/keywords/get_clusters/', keywords_get_clusters)
app.router.add_route('POST', '/keywords/get_category_clusters/', keywords_get_category_clusters)

app.router.add_route('POST', '/_loopback', loopback)
app.router.add_route('GET', '/help', get_help)

setup_swagger(app)
