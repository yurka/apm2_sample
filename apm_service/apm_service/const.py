class TaskStatus:
    NEW = 'new'
    RUNNING = 'running'
    PENDING = 'pending'
    COMPLETED = 'completed'
    ERROR = 'error'