# import os

# import copy
# import logging

# import aiohttp
# import colander
# import funcy


# logger = logging.getLogger(__name__)


# _task_handler_registry = {}


# class TaskSchema(colander.MappingSchema):
#     name = colander.SchemaNode(colander.String())
#     callback = colander.SchemaNode(colander.String(), validator=colander.url, missing=None)


# class TaskHandler:
#     """
#     Task processing class
#     """
#     schema = TaskSchema()

#     def __init__(self, task):
#         self.task = task

#     async def make_parser_params(self):
#         raise NotImplementedError()

#     async def process_parser_result(self, result):
#         raise NotImplementedError()

#     def validate_task_input(self, task):
#         try:
#             self.schema.deserialize(task)
#         except colander.Invalid as e:
#             return e.asdict()


# def get_task_handler(task):
#     """
#     get task class for task
#     """
#     handler_class = _task_handler_registry.get(task["name"])
#     if handler_class is None:
#         raise ValueError("No such task: {}".format(task["name"]))
#     return handler_class(task)


# class OriginalityParamsSchema(colander.MappingSchema):
#     text = colander.SchemaNode(colander.String())
#     shingle_size = colander.SchemaNode(colander.Int(), validator=colander.OneOf([2, 3, 4]))
#     complete_threshold = colander.SchemaNode(colander.Float(),
#                                              validator=colander.Range(0, 100),
#                                              missing=100)


# class OriginalityTaskSchema(TaskSchema):
#     params = OriginalityParamsSchema()


# class OriginalityTaskHandler(TaskHandler):
#     """
#     """
#     PRESET_NAME = '№ 887'
#     schema = OriginalityTaskSchema()

#     # TODO: validate user input
#     async def make_parser_params(self):

#         params = copy.deepcopy(self.task['params'])
#         params.setdefault('config_preset', 'default')
#         params['preset'] = self.PRESET_NAME
#         params['queries'] = self._make_shingles(self.task['params']['text'],
#                                                 int(self.task['params']['shingle_size']))
#         params['queries_from'] = 'text'
#         return params

#     async def process_parser_result(self, parser_result):
#         logger.debug("Process task #%s result", self.task['params'])
#         cnt = 0
#         false_cnt = 0
#         for item in parser_result:
#             if not item:
#                 continue
#             cnt += 1
#             if list(item.values())[0] == 'false':
#                 false_cnt += 1
#         return {"originality": false_cnt / cnt * 100}

#     def _make_shingles(self, text, chunk_size):
#         chunks_iter = funcy.chunks(chunk_size, text.split())
#         return [" ".join(c) for c in chunks_iter]


# class SocialSignalParamsSchema(colander.MappingSchema):
#     id = colander.SchemaNode(colander.Int())
#     limit = colander.SchemaNode(colander.Int())
#     api_key = colander.SchemaNode(colander.String())


# class SocialSignalTaskSchema(TaskSchema):
#     params = SocialSignalParamsSchema()


# class SocialSignalTaskHandler(TaskHandler):
#     """
#     """
#     schema = SocialSignalTaskSchema()

#     API_URL = ('http://ds.wiseweb.co/api/v1/origins/{id}/records?limit={limit}',
#                '&columns[0]=url_source&hide_system_columns=1&api_key={api_key}')

#     async def make_parser_params(self):
#         queries = await self._fetch_urls()
#         params = {
#           "resultsFileName": "$datefile.format().txt",
#           "parsers": [['Rank::Social::Signal', 'default']],
#           "uniqueQueries": 0,
#           "keepUnique": 0,
#           "resultsPrepend": "",
#           "queries": queries,
#           "config_preset": "200threads",
#           "moreOptions": 0,
#           "queries_from": "text",
#           "resultsUnique": "no",
#           "doLog": "no",
#           "queryFormat": "$query",
#           "resultsSaveTo": "file",
#           "configOverrides": [],
#           "resultsFormat": "$p1.preset",
#           "resultsAppend": "",
#           "queryBuilders": []
#         }
#         params['preset'] = None
#         return params

#     async def process_parser_result(self, parser_result):
#         logger.debug("Process task #%s result", self.task['params'])
#         return self.parse_result(parser_result)

#     def parse_result(self, result_text):
#         result = {}
#         url = ""
#         for line in result_text.split('\n'):
#             if not line.strip():
#                 continue
#             if line.startswith('http'):
#                 url = line
#                 result[url] = {}
#             else:
#                 k, v = line.split(':')
#                 result[url][k] = int(v) if v != ' none' else None
#         return result


# async def make_parser_params(task):
#     return await get_task_handler(task).make_parser_params()


# async def process_parser_result(task, parser_result):
#     return await get_task_handler(task).process_parser_result(parser_result)


# def validate_task_input(task):
#     if not task.get('name'):
#         return {'name': 'Reqired'}
#     if not task.get('name') in _task_handler_registry:
#         return {'name': 'Should be one of {}'.format(list(_task_handler_registry.keys()))}
#     return get_task_handler(task).validate_task_input(task)



# class GoogleTrendsParamsSchema(colander.MappingSchema):
#     keywords = colander.Sequence(colander.String())


# class GoogleTrendsTaskSchema(TaskSchema):
#     params = GoogleTrendsParamsSchema()


# class GoogleTrendsTaskHandler(TaskHandler):
#     """
#     """
#     schema = GoogleTrendsTaskSchema()

#     # TODO: validate user input
#     async def make_parser_params(self):
#         queries = self.task['params']['keywords']
#         params = {
#           "resultsFileName": "$datefile.format().txt",
#           "parsers": [['SE::Google::Trends', 'default']],
#           "uniqueQueries": 0,
#           "keepUnique": 0,
#           "resultsPrepend": "",
#           "queries": queries,
#           "config_preset": "200threads",
#           "moreOptions": 0,
#           "queries_from": "text",
#           "resultsUnique": "no",
#           "doLog": "no",
#           "queryFormat": "$query",
#           "resultsSaveTo": "file",
#           "configOverrides": [],
#           "resultsFormat": "$p1.preset",
#           "resultsAppend": "",
#           "queryBuilders": []
#         }
#         params['preset'] = None
#         return params

#     async def process_parser_result(self, parser_result):
#         logger.debug("Process task #%s result", self.task['params'])
#         cnt = 0
#         false_cnt = 0
#         for item in parser_result:
#             if not item:
#                 continue
#             cnt += 1
#             if list(item.values())[0] == 'false':
#                 false_cnt += 1
#         return {"originality": false_cnt / cnt * 100}

#     def _make_shingles(self, text, chunk_size):
#         chunks_iter = funcy.chunks(chunk_size, text.split())
#         return [" ".join(c) for c in chunks_iter]

# _task_handler_registry['originality'] = OriginalityTaskHandler
# _task_handler_registry['social_signal'] = SocialSignalTaskHandler
