import logging
import uuid

from aiopg.sa import create_engine
import sqlalchemy as sa
from sqlalchemy.dialects.postgresql import UUID, JSONB


from datetime import datetime

from apm_service.const import TaskStatus


logger = logging.getLogger(__name__)

logging.getLogger('sqlalchemy.engine').setLevel(logging.DEBUG)
logging.getLogger('aiopg').setLevel(logging.DEBUG)


metadata = sa.MetaData()

"""
 id        | integer                | not null default nextval('main_parser_id_seq'::regclass)
 url       | character varying(200) | not null
 password  | character varying(200) | not null
 is_active | boolean                | not null
 status    | character varying(200) | not null
"""
parsers = sa.Table('main_parser', metadata,
                   sa.Column('id', sa.Integer, primary_key=True),
                   sa.Column('url', sa.String(200), nullable=False),
                   sa.Column('password', sa.String(200), nullable=False),
                   sa.Column('is_active', sa.Boolean, nullable=False),
                   sa.Column('status', sa.String(200), nullable=False),
                   sa.Column('slots_number', sa.Integer),
                   )

"""
main_task:
 id           | uuid                     | not null
 ext_id       | character varying(200)   |
 name         | character varying(200)   | not null
 params       | jsonb                    | not null
 status       | character varying(200)   | not null
 result       | jsonb                    | not null
 created_at   | timestamp with time zone | not null
 enqueued_at  | timestamp with time zone |
 completed_at | timestamp with time zone |
 parser_id    | integer                  |
"""
tasks = sa.Table('main_task', metadata,
                 sa.Column('id', UUID(), primary_key=True),
                 sa.Column('ext_id', sa.Integer),
                 sa.Column('name', sa.String(200), nullable=False),
                 sa.Column('params', JSONB, default=dict, nullable=False),
                 sa.Column('status', sa.String(200), nullable=False),
                 sa.Column('result', JSONB, default=dict, nullable=False),
                 sa.Column('parser_result', JSONB, default=dict, nullable=False),
                 sa.Column('parser_status', JSONB, default=dict, nullable=False),
                 sa.Column('created_at', sa.DateTime, nullable=False, default=datetime.now),
                 sa.Column('enqueued_at', sa.DateTime),
                 sa.Column('completed_at', sa.DateTime),
                 sa.Column('name', sa.String(200), nullable=False),
                 sa.Column('parser_id', sa.Integer, sa.ForeignKey('main_parser.id')),
                 sa.Column('callback', sa.String(500), nullable=True),
                 sa.Column('callback_done', sa.Boolean, nullable=False, default=False),
                 sa.Column('callback_http_status', sa.Integer, nullable=True),
                 sa.Column('add_task_retries', sa.Integer, default=0),
                 sa.Column('callback_retries', sa.Integer, default=0),
                 sa.Column('on_error_retries', sa.Integer, default=0),
                 )


originality_stoplists = sa.Table('main_originalitystoplist', metadata,
                                 sa.Column('id', sa.Integer, primary_key=True),
                                 sa.Column('name', sa.String(200), nullable=False),
                                 sa.Column('words', sa.Text),
                                 )


class Storage:
    """
    All-in-one Database interface
    """

    def __init__(self, service, dsn):
        self.service = service
        self.dsn = dsn

    async def connect(self):
        self.engine = await create_engine(self.dsn)

    async def put_task(self, task):
        task_id = str(uuid.uuid4())

        task['id'] = task_id
        task['status'] = TaskStatus.NEW
        async with self.engine.acquire() as conn:
            await conn.execute(tasks.insert().values(**task))
        return task_id

    async def get_task(self, task_id):
        async with self.engine.acquire() as conn:
            res = await conn.execute(tasks.select().where(tasks.c.id == task_id))
            task = await res.first()
            if task:
                return dict(task)

    async def update_task(self, task_id, values):
        query = tasks.update().values(**values).where(tasks.c.id == task_id)
        async with self.engine.acquire() as conn:
            await conn.execute(query)
        return task_id

    async def task_set_status_running(self, task_id, task_ext_id, parser_id):
        updates = {'status': TaskStatus.RUNNING,
                   'ext_id': task_ext_id,
                   'enqueued_at': datetime.now(),
                   'parser_id': parser_id}
        return await self.update_task(task_id, updates)

    async def task_set_status_pending(self, task_id):
        updates = {'status': TaskStatus.PENDING}
        return await self.update_task(task_id, updates)

    async def task_set_result(self, task_id, result, parser_result):
        values = dict(result=result,
                      parser_result=parser_result,
                      status=TaskStatus.COMPLETED,
                      completed_at=datetime.now())
        return await self.update_task(task_id, values)

    async def task_set_error(self, task_id, parser_status):
        updates = {'status': TaskStatus.ERROR,
                   'parser_status': parser_status}
        return await self.update_task(task_id, updates)

    async def get_tasks_to_run(self, limit, exclude=None):
        if exclude:
            condition = sa.and_(tasks.c.status == TaskStatus.NEW, ~tasks.c.id.in_(exclude))
        else:
            condition = tasks.c.status == TaskStatus.NEW
        query = tasks.select().where(condition).order_by(tasks.c.created_at.desc()).limit(limit)
        return await self.fetch_query_result_list(query)

    async def get_running_tasks(self):
        query = tasks.select().where(
            tasks.c.status == TaskStatus.RUNNING).order_by(tasks.c.created_at.desc())
        return await self.fetch_query_result_list(query)

    async def get_pending_tasks(self):
        query = tasks.select().where(
            tasks.c.status == TaskStatus.PENDING).order_by(tasks.c.created_at.desc())
        return await self.fetch_query_result_list(query)

    async def get_ready_for_callback_tasks(self):
        query = tasks.select().where(sa.and_(
            tasks.c.status == TaskStatus.COMPLETED,
            tasks.c.callback != None,
            tasks.c.callback_done == False)).order_by(tasks.c.created_at.desc())
        return await self.fetch_query_result_list(query)

    async def fetch_query_result_list(self, query):
        async with self.engine.acquire() as conn:
            res = await conn.execute(query)
            data = await res.fetchall()
            return [dict(r) for r in data]

    async def get_parsers(self):
        query = parsers.select().where(parsers.c.is_active == True)
        async with self.engine.acquire() as conn:
            res = await conn.execute(query)
            data = await res.fetchall()
            return [dict(r) for r in data]

    async def task_set_callback_done(self, task_id, http_status):
        updates = {'callback_done': True, 'callback_http_status': http_status}
        return await self.update_task(task_id, updates)

    async def task_retry_on_error(self, task):
        updates = {'on_error_retries': task['on_error_retries'] + 1, 'status': TaskStatus.NEW}
        return await self.update_task(task['id'], updates)

    async def originality_get_stoplist(self, stoplist_name):
        query = originality_stoplists.select().where(
            originality_stoplists.c.name == stoplist_name)
        res = await self.fetch_query_result_list(query)
        if not res:
            return None
        else:
            return res[0]
