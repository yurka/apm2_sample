import asyncio
import logging

logger = logging.getLogger(__name__)


class Scheduler:
    """
    Scheduler is responible to manage tasks queue.

    Scheduler runs 2 loops (coros):

    Main loop will try to:
        - fetch tasks with status 'new' from DB and put them to inner queue
        - fetch tasks with status 'running' from DB and call manager to check its state in parser
        - fetch tasks with status 'pending' from DB and call manager to get task result from parser
        - fetch tasks with status 'complete' from DB and call manager to do callback for task

    consumer_loop loop will try to:
        - get idle parser from manager
        - get task from inner queue
        - call parser to run task
    """

    def __init__(self, service, period=2):
        self.service = service
        self.period = period
        self.queue = asyncio.Queue()
        self.queue_max_size = 10
        self.scheduling_tasks = set()

    def run(self):
        asyncio.ensure_future(self.consumer_loop())
        asyncio.ensure_future(self.main_loop())

    async def main_loop(self):
        while True:
            await asyncio.sleep(self.period)
            try:
                logger.info("scheduler main loop tick, queue size %s", self.queue.qsize())
                await self._main_loop_tact()
            except Exception:
                logger.exception("Scheduler main loop error")

    async def _main_loop_tact(self):
        current_queue_size = self.queue.qsize()
        if current_queue_size < self.queue_max_size / 2:
            tasks_to_run = await self.service.storage.get_tasks_to_run(
                limit=self.queue_max_size - current_queue_size,
                exclude=list(self.scheduling_tasks))
            self.scheduling_tasks.update([t['id'] for t in tasks_to_run])
            if tasks_to_run is None:
                logger.debug("No tasks to run")
            else:
                for task in tasks_to_run:
                    await self.queue.put(task)

        running_tasks = await self.service.storage.get_running_tasks()
        logger.debug("Running tasks: %s", running_tasks)
        for task in running_tasks:
            asyncio.ensure_future(self.service.manager.check_task(task))

        pending_tasks = await self.service.storage.get_pending_tasks()
        logger.debug("Pending tasks: %s", pending_tasks)
        for task in pending_tasks:
            asyncio.ensure_future(self.service.manager.get_result(task))

        ready_for_callback_tasks = await self.service.storage.get_ready_for_callback_tasks()
        logger.debug("Ready for callback tasks: %s", ready_for_callback_tasks)
        for task in ready_for_callback_tasks:
            asyncio.ensure_future(self.service.manager.callback(task))

    async def consumer_loop(self):
        while True:
            try:
                logger.debug("scheduler consumer loop tick, queue size %s", self.queue.qsize())
                await self._consumer_loop_tact()
            except Exception:
                logger.exception("Scheduler consumer loop error")

    async def _consumer_loop_tact(self):
        if self.queue.qsize() == 0:
            await asyncio.sleep(self.period)
            return
        parser = await self.service.manager.get_idle_parser()
        if parser is None:
            logger.info("No idle parsers")
            await asyncio.sleep(self.period)
            return
        else:
            task_to_run = await self.queue.get()
            logger.debug("Consume from queue %s", task_to_run['id'])
            asyncio.ensure_future(parser.run_task(task_to_run, self.notify_task_runned))

    def notify_task_runned(self, task_id):
        try:
            self.scheduling_tasks.remove(task_id)
        except KeyError:
            logger.exception("Task is not in scheduled")
