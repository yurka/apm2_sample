import asyncio
import copy
import json
import logging

import aiohttp


logger = logging.getLogger(__name__)


class AParserTaskStatus:
    """
    starting, pausing, stopping, deleting, error
    """
    STARTING = 'starting'
    PAUSING = 'pausing'
    STOPPING = 'stopping'
    DELETING = 'deleting'
    COMPLETED = 'completed'
    ERROR = 'error'

    @property
    def all(self):
        return (self.STARTING, self.PAUSING, self.STOPPING,
                self.DELETING, self.ERROR, self.COMPLETED)


class ApiError(Exception):
    """
    Base AParser API client error
    """
    def __init__(self, message, http_status=None):
        self.http_status = http_status
        self.message = message

    def __str__(self):
        return "{}, status: {}".format(self.message, self.http_status)


class ApiClientError(ApiError):
    """
    Client error - connection/request/response errors
    or 4xx HTTP response status from server
    """


class ApiServerError(ApiError):
    """
    When parser returns 'success'!= 1 or 5xx HTTP response status from server
    """


class AParser:
    """
    AParser User API client.
    Asyncio port of https://github.com/EitherSoft/PyAParser
    API docs: http://a-parser.com/wiki/user-api/

    ping                        - ping
    info                        - info
    one_request                 - oneRequest
    bulk_request                - bulkRequest
    get_parser_preset           - getParserPreset
    get_proxies                 - getProxies
    set_proxy_checker_preset    - setProxyCheckerPreset
    add_task                    - addTask
    get_task_state              - getTaskState
    get_task_conf               - getTaskConf
    get_task_result_file        - getTaskResultsFile
    delete_task_result_file     - deleteTaskResultsFile
    change_task_status          - changeTaskStatus
    move_task                   - moveTask
    get_tasks_list              - getTasksList
    get_parser_info             - getParserInfo
    """
    api_url = None
    password = None

    def __init__(self, api_url, password, loop=None):
        self.api_url = api_url
        self.password = password
        self.loop = loop

    async def _do_request(self, action, data=None, opts=None):
        request = {
          "password": self.password,
          "action": action
        }

        if isinstance(data, dict):
            if isinstance(opts, dict):
                data.update(opts)
            request['data'] = data
        try:
            async with aiohttp.ClientSession(loop=self.loop) as client:
                request_copy = copy.deepcopy(request)
                request_copy['password'] = '*****'
                try:
                    request_copy['data']['queries'] = request_copy['data']['queries'][:3]
                except:
                    pass
                request = json.dumps(request)
                logger.debug("API request url: %s data: %s", self.api_url, request_copy)
                async with client.post(self.api_url, data=request) as resp:
                    logger.debug("API response url: %s status: %s", self.api_url, resp.status)
                    response_text = await resp.text()
                    response_status = resp.status
        except aiohttp.ClientError as e:
            logger.exception("HTTP Error")
            raise ApiClientError(str(e))
        if response_status // 100 == 4:
            raise ApiClientError(response_text, response_status)
        if response_status // 100 == 5:
            raise ApiServerError(response_text, response_status)
        try:
            response = json.loads(response_text)
        except Exception as e:
            logger.exception("Can't parse server response: %s", response_text)
            raise ApiServerError(response_text, response_status)

        if response['success']:
            return response.get('data')
        else:
            logger.error("API response fail: %s %s success: %s",
                         self.api_url, request_copy, response)
            raise ApiServerError(response_text, response_status)

    async def ping(self):
        return await self._do_request('ping')

    async def info(self):
        return await self._do_request('info')

    async def get_parser_info(self, parser):
        data = {
            'parser': parser,
        }
        return await self._do_request('getParserInfo', data)

    async def get_proxies(self):
        return await self._do_request('getProxies')

    async def get_parser_preset(self, parser, preset):
        data = {
            'parser': parser,
            'preset': preset,
        }
        return await self._do_request('getParserPreset', data)

    async def one_request(self, parser, preset, query, **kwargs):
        data = {
            'parser': parser,
            'preset': preset,
            'query': query
        }
        return await self._do_request('oneRequest', data, kwargs)

    async def bulk_request(self, parser, preset, threads, queries, **kwargs):
        data = {
            'parser': parser,
            'preset': preset,
            'queries': queries,
            'threads': threads
        }
        return await self._do_request('bulkRequest', data, kwargs)

    async def add_task(self, config_preset, preset, queries_from, queries, **kwargs):
        data = {
            'configPreset': config_preset,
            'preset': preset,
            'queriesFrom': queries_from,
            'queries' if queries_from == 'text' else 'queriesFile': queries,
        }
        return await self._do_request('addTask', data, kwargs)

    async def get_tasks_list(self, completed=0):
        data = {
            'completed': completed
        }
        return await self._do_request('getTasksList', data)

    async def get_task_state(self, task_id):
        data = {
            'taskUid': task_id,
        }
        return await self._do_request('getTaskState', data)

    async def get_task_conf(self, task_id):
        data = {
            'taskUid': task_id,
        }
        return await self._do_request('getTaskConf', data)

    async def change_task_status(self, task_id, to_status):
        # starting|pausing|stopping|deleting
        data = {
            'taskUid': task_id,
            'toStatus': to_status
        }
        return await self._do_request('changeTaskStatus', data)

    async def move_task(self, task_id, direction):
        # start|end|up|down
        data = {
            'taskUid': task_id,
            'direction': direction
        }
        return await self._do_request('moveTask', data)

    async def set_proxy_checker_preset(self, preset):
        data = {
            'preset': preset
        }
        return await self._do_request('setProxyCheckerPreset', data)

    async def get_task_result_file(self, task_id):
        data = {
            'taskUid': task_id
        }
        return await self._do_request('getTaskResultsFile', data)

    async def delete_task_result_file(self, task_id):
        data = {
            'taskUid': task_id
        }

        return await self._do_request('getTaskResultsFile', data)

    async def wait_for_task(self, task_id, interval=5):
        while True:
            state = await self.get_task_state(task_id)

            if not state or state['status'] == 'completed':
                return state
            asyncio.sleep(interval)

    async def get_task_result(self, task_id):
        url = await self.get_task_result_file(task_id)
        async with aiohttp.ClientSession(loop=self.loop) as client:
            logger.debug("API: %s getting result file: %s", self.api_url, url)
            async with client.get(url) as resp:
                response_text = await resp.text()
                logger.debug("API: %s status: %s", self.api_url, resp.status)
        try:
            return json.loads(response_text)
        except json.JSONDecodeError:
            try:
                # if task not completed, result text is not finlized, try to finalize
                return json.loads(response_text + '{}]')
            except json.JSONDecodeError:
                return response_text
