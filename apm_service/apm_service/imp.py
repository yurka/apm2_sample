from impala.dbapi import connect

# http://cloudera01.wiseweb.co:25020
# cloudera02.wiseweb.co 21050
# conn = connect(host='cloudera01.wiseweb.co', port=21050)
# conn = connect(host='cloudera02.wiseweb.co', port=21050)

conn = connect(host='localhost', port=21050)
cursor = conn.cursor()

cursor.execute('SELECT * FROM parser_results LIMIT 100')
print(cursor.description)  # prints the result set's schema
results = cursor.fetchall()
print(results)  # prints the result set's schema


def create_parser_results_table():
    # id - row_number()    
    sql = """CREATE TABLE parser_results (
                `task_id` INT,
                 `Keyword` STRING,
                 `Type` STRING,
                 `Position` INT,
                 `Url` STRING,
                 `Text` STRING,
                 `UpdatedAt` TIMESTAMP) STORED AS PARQUET"""
    cursor.execute(sql)


# create_parser_results_table()


