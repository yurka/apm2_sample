import logging

import aiohttp
import colander


from .base import TaskSchema, TaskHandler
from .registry import registry


logger = logging.getLogger(__name__)


class SocialSignalParamsSchema(colander.MappingSchema):
    id = colander.SchemaNode(colander.Int(), description="URLs resource ID")
    limit = colander.SchemaNode(
        colander.Int(),
        description="limit API parameter")
    api_key = colander.SchemaNode(colander.String(), description="API key")


class SocialSignalTaskSchema(TaskSchema):
    params = SocialSignalParamsSchema()


@registry.register_task('social_signal')
class SocialSignalTaskHandler(TaskHandler):
    """
    use Rank::Social::Signal parser
    """
    schema = SocialSignalTaskSchema()

    API_URL = ('http://ds.wiseweb.co/api/v1/origins/{id}/records?limit={limit}'
               '&columns[0]=url_source&hide_system_columns=1&api_key={api_key}')

    async def make_parser_params(self):
        queries = await self._fetch_urls()
        params = {
            "resultsFileName": "$datefile.format().txt",
            "parsers": [['Rank::Social::Signal', 'default']],
            "uniqueQueries": 0,
            "keepUnique": 0,
            "resultsPrepend": "",
            "queries": queries,
            "config_preset": "200threads",
            "moreOptions": 0,
            "queries_from": "text",
            "resultsUnique": "no",
            "doLog": "no",
            "queryFormat": "$query",
            "resultsSaveTo": "file",
            "configOverrides": [],
            "resultsFormat": "$p1.preset",
            "resultsAppend": "",
            "queryBuilders": []
        }
        params['preset'] = None
        return params

    async def process_parser_result(self, parser_result):
        logger.debug("Process task #%s result", self.request['params'])
        return self.parse_result(parser_result)

    def parse_result(self, result_text):
        result = {}
        url = ""
        for line in result_text.split('\n'):
            if not line.strip():
                continue
            if line.startswith('http'):
                url = line
                result[url] = {}
            else:
                k, v = line.split(':')
                result[url][k] = int(v) if v != ' none' else None
        return result

    async def _fetch_urls(self):
        url = self.API_URL.format(**self.request['params'])
        async with aiohttp.ClientSession() as client:
            async with client.get(url) as resp:
                if resp.status != 200:
                    raise RuntimeError(
                        "social_signal task: URL API response status {}".format(resp.status))

                data = await resp.json()
        return [i['url_source'] for i in data]
