

class HandlerRegistry:
    """
    Registry for Task and BulkRequest handlers
    """
    def __init__(self):
        self._tasks = {}
        self._bulk_requests = {}

    def register_task(self, name):
        def _register(handler_class):
            if name in self._tasks:
                raise RuntimeError("Task Handler '{}' already registered".format(name))
            self._tasks[name] = handler_class
            return handler_class
        return _register

    def get_task_handler(self, task, **kw):
        """
        get task hanler instance for task
        """
        handler_class = self._tasks.get(task["name"])
        if handler_class is None:
            raise ValueError("No such Task Handler: {}".format(task["name"]))
        return handler_class(task, **kw)

    def register_bulk_request(self, name):
        def _register(handler_class):
            if name in self._bulk_requests:
                raise RuntimeError("Bulk Request Handler '{}' already registered".format(name))
            self._bulk_requests[name] = handler_class
            return handler_class
        return _register

    def get_bulk_request_handler(self, request):
        """
        get bulk request hanler instance for task
        """
        handler_class = self._bulk_requests.get(request["name"])
        if handler_class is None:
            raise ValueError("No such Bulk Request Handler: {}".format(request["name"]))
        return handler_class(request)

    def validate_bulk_request(self, request):
        name = request.get('name')
        if not name:
            return {'name': 'Reqired'}
        if name not in self._bulk_requests:
            return {'name': 'Should be one of {}'.format(list(self._bulk_requests.keys()))}
        return self._bulk_requests[name].validate_request(request)

    def validate_task(self, request):
        name = request.get('name')
        if not name:
            return {'name': 'Reqired'}
        if name not in self._tasks:
            return {'name': 'Should be one of {}'.format(list(self._tasks.keys()))}
        return self._tasks[name].validate_request(request)

    def describe(self):
        """
        describe all registered handlers and its params
        """
        description = {'tasks': [], 'bulk_requests': []}
        for name, handler in self._tasks.items():
            task_descr = {
                'name': name,
                'description': handler.__doc__,
                'parameters': handler.describe_schema()['params']
            }
            description['tasks'].append(task_descr)
        for name, handler in self._bulk_requests.items():
            task_descr = {
                'name': name,
                'description': handler.__doc__,
                'parameters': handler.describe_schema()['params']
            }
            description['bulk_requests'].append(task_descr)
        return description


registry = HandlerRegistry()
