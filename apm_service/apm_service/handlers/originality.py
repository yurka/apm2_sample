import copy
import collections
import logging

import colander
import funcy


from .base import TaskSchema, TaskHandler
from .registry import registry


logger = logging.getLogger(__name__)


class OriginalityParamsSchema(colander.MappingSchema):
    text = colander.SchemaNode(colander.String(), description="Text to check")
    shingle_size = colander.SchemaNode(colander.Int(), validator=colander.OneOf([2, 3, 4]),
                                       description="size of shingle - 2, 3 or 4 words")
    overlap = colander.SchemaNode(colander.Boolean(), missing=False,
                                  description="use overlapping shingles (one word overlap)")
    stop_list = colander.SchemaNode(colander.String(), missing='default',
                                    description="stop-words list name")
    complete_threshold = colander.SchemaNode(
        colander.Float(),
        validator=colander.Range(0, 100),
        missing=100,
        description="% of successful requests when task is considered completed")
    top_urls = colander.SchemaNode(colander.Int(), validator=colander.Range(min=1),
                                   missing=None,
                                   description="return a list of top finded URLs")


class OriginalityTaskSchema(TaskSchema):
    params = OriginalityParamsSchema()


@registry.register_task('originality')
class OriginalityTaskHandler(TaskHandler):
    """
    Check text originality
    """
    PRESET_NAME = '№ 887'
    schema = OriginalityTaskSchema()

    async def make_parser_params(self):
        params = copy.deepcopy(self.request['params'])
        params.setdefault('config_preset', '200threads')
        params['preset'] = self.PRESET_NAME
        text = await self.apply_stoplist(self.request['params']['text'],
                                         self.request['params']['stop_list'])
        params['queries'] = self._make_shingles(text,
                                                self.request['params']['shingle_size'],
                                                self.request['params']['overlap']
                                                )
        params['queries_from'] = 'text'
        return params

    async def process_parser_result(self, parser_result):
        logger.debug("Process task #%s result", self.request['params'])
        cnt = 0
        false_cnt = 0

        for item in parser_result:
            if not item:
                continue
            cnt += 1
            if list(item.values())[0] == 'false':
                false_cnt += 1
        originality = false_cnt / cnt * 100
        result = {"originality": originality}
        if originality < 100:
            result['urls'] = self._transform_result(parser_result)
        return result

    async def apply_stoplist(self, text, stop_list):
        storage = self.options['storage']
        stop_list_data = await storage.originality_get_stoplist(stop_list)
        if stop_list_data is None:
            raise RuntimeError('Stop-list "{}" does not exist'.format(stop_list))
        else:
            stop_words = set(stop_list_data['words'].split())
            result = []
            for word in text.split():
                if word not in stop_words:
                    result.append(word)
            return ' '.join(result)

    def _make_shingles(self, text, chunk_size, use_overlap):

        def chunks_overlap(words, chunk_size, overlap):
            if overlap > 0:
                return [words[i:i + chunk_size - overlap + 1]
                        for i in range(0, len(words), chunk_size - overlap)]
            else:
                return [words[i:i + chunk_size] for i in range(0, len(words), chunk_size)]

        chunks_list = chunks_overlap(text.split(), chunk_size, overlap=int(use_overlap))
        if len(chunks_list[-1]) < chunk_size:
            chunks_list.pop()
        return [" ".join(c) for c in chunks_list]

    def _transform_result(self, result):
        top_count = self.request['params']['top_urls']
        if top_count is None:
            return result
        url_counter = collections.Counter()
        search_list = []
        for item in result:
            if not item:
                continue
            for url_list in item.values():
                url_counter.update(url_list)
            for shingle, urls in item.items():
                search_list.append((shingle, set(urls)))
        top_urls = [i[0] for i in url_counter.most_common(top_count)]
        res = []
        for url in top_urls:
            shingles_list = []
            for item in search_list:
                if url in item[1]:
                    shingles_list.append(item[0])
            res.append({'url': url, 'shingles': shingles_list})
        return res
