import logging

import colander


from .base import BaseSchema, BulkRequestHandler
from .registry import registry


logger = logging.getLogger(__name__)


class SemrushKeywordsParamsSchema(colander.MappingSchema):
    keywords = colander.SchemaNode(colander.List(), validator=colander.Length(min=1),
                                   description="keywords list")


class SemrushKeywordsSchema(BaseSchema):
    params = SemrushKeywordsParamsSchema()


@registry.register_bulk_request('semrush_keywords')
class SemrushKeywordsHandler(BulkRequestHandler):
    """
    user Rank::SEMrush::Keyword parser
    """
    schema = SemrushKeywordsSchema()

    async def make_parser_params(self):
        queries = self.request['params']['keywords']

        params = {
            "parser": "Rank::SEMrush::Keyword",
            "preset": "default",
            "threads": 500,
            "rawResults": 1,
            "queries": queries
        }
        return params

    async def process_parser_result(self, parser_result):
        logger.debug("Process task #%s result", self.request['params'])
        return parser_result['results']
