import colander


class BaseSchema(colander.MappingSchema):
    name = colander.SchemaNode(colander.String())


class TaskSchema(BaseSchema):
    callback = colander.SchemaNode(colander.String(), validator=colander.url, missing=None)


class BaseHandler:
    """
    Task or Bulk Request processing class
    """
    schema = BaseSchema()

    def __init__(self, request, **kw):
        self.request = self.schema.deserialize(request)
        self.options = kw

    async def make_parser_params(self):
        raise NotImplementedError()

    async def process_parser_result(self, result):
        raise NotImplementedError()

    @classmethod
    def validate_request(cls, request):
        try:
            cls.schema.deserialize(request)
        except colander.Invalid as e:
            return e.asdict()

    @classmethod
    def describe_schema(cls):
        return describe_schema(cls.schema)


def describe_schema(schema):
    description = {}
    for node in schema.children:
        if isinstance(node.typ, colander.Mapping):
            descr = describe_schema(node)
        else:
            descr = {}
            descr['type'] = node.typ.__class__.__name__
            descr['required'] = node.required
            descr['description'] = node.description
            if not node.required:
                descr['default'] = node.missing
        description[node.name] = descr
    return description


class TaskHandler(BaseHandler):
    schema = TaskSchema()


class BulkRequestHandler(BaseHandler):
    pass
