import logging

import colander


from .base import BaseSchema, BulkRequestHandler
from .registry import registry


logger = logging.getLogger(__name__)


class GoogleTrendsParamsSchema(colander.MappingSchema):
    keywords = colander.SchemaNode(colander.List(), validator=colander.Length(min=1),
                                   description="keywords list")
    search_property = colander.SchemaNode(colander.String(), missing=None,
                                          description="search type filter")
    search_time = colander.SchemaNode(colander.String(), missing=None,
                                      description="date period filter")
    search_category = colander.SchemaNode(colander.Int(), missing=None,
                                          description="category filter")
    search_country = colander.SchemaNode(colander.String(), missing=None,
                                         description="country filter")


class GoogleTrendsSchema(BaseSchema):
    params = GoogleTrendsParamsSchema()


@registry.register_bulk_request('google_trends')
class GoogleTrendsHandler(BulkRequestHandler):
    """
    use SE::Google::Trends parser
    """
    schema = GoogleTrendsSchema()

    async def make_parser_params(self):
        queries = self.request['params']['keywords']
        options = self._make_options()
        params = {
            "parser": "SE::Google::Trends",
            "preset": "default",
            "threads": 500,
            "rawResults": 1,
            "queries": queries,
            "options": options,
        }
        return params

    async def process_parser_result(self, parser_result):
        logger.debug("Process task #%s result", self.request['params'])
        return parser_result['results']

    def _make_options(self):
        options = []
        for k, v in self.request['params'].items():
            if k == "keywords" or v is None:
                continue
            options.append({'value': v, 'id': k, 'type': 'override'})
        return options
