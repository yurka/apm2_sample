import apm_service.handlers.social_signal
import apm_service.handlers.originality
import apm_service.handlers.google_trends
import apm_service.handlers.semrush_keywords
from .registry import registry
