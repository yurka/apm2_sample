import asyncio
import logging


from aiohttp import web

from .api import app
from .manager import Manager
from .monitor import Monitor
from .scheduler import Scheduler
from .storage import Storage


logger = logging.getLogger(__name__)


class Service:
    def __init__(self, options):
        self.options = options
        self.storage = Storage(self, options.db_dsn)
        self.manager = Manager(self)
        self.monitor = Monitor(self)
        self.scheduler = Scheduler(self)

    def run(self):
        """
        Start all process
        """
        logger.info("Starting Aparser Manager")

        loop = asyncio.get_event_loop()

        loop.run_until_complete(self.storage.connect())

        asyncio.ensure_future(self.manager.run())
        asyncio.ensure_future(self.monitor.run())
        self.scheduler.run()

        app.storage = self.storage
        app.manager = self.manager
        app.service = self
        print('loop', app.loop)

        host, port = self.options.api_bind.split(':')
        web.run_app(app, host=host, port=int(port))
