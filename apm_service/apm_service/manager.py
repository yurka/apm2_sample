import asyncio
import itertools
import logging

import aiohttp

from apm_service.aparser_client import AParser, AParserTaskStatus, ApiServerError, ApiClientError
from apm_service.const import TaskStatus
from apm_service.handlers import registry

logger = logging.getLogger(__name__)


ADD_TASK_MAX_RETRIES = 3
ON_ERROR_MAX_RETRIES = 2
CALLBACK_MAX_RETRIES = 3


class Parser:
    """
    Parser instance representation
    """

    def __init__(self, parser_id, url, password, slots_number, storage):
        self.id = parser_id
        self.url = url
        self.password = password
        self.storage = storage
        api_url = url + 'API'
        self.client = AParser(api_url, password)
        self.free_slots = 0
        self.total_slots = slots_number

    async def check(self):
        self.free_slots = 0
        try:
            info = await self.get_info()
        except ApiClientError:
            return
        self.free_slots = self.total_slots - info['tasksInQueue']
        if self.free_slots < 0:
            self.free_slots = 0

    async def run_task(self, task, success_callback):
        logger.info('Run task %s', task['id'])
        # TODO: fix double enqueuing
        try:
            task_kwargs = await registry.get_task_handler(task, storage=self.storage).make_parser_params()
            logger.debug("Task Kwargs: %s", task_kwargs)
            task_ext_id = await self.client.add_task(**task_kwargs)
        except Exception as e:
            logger.exception('Task %s failed to added', task['id'])
            if task['add_task_retries'] >= ADD_TASK_MAX_RETRIES:
                await self.storage.task_set_error(task['id'], {'add_task_error': str(e)})
            raise
        else:
            logger.info('Task %s added, ext_id: %s', task['id'], task_ext_id)
            await self.storage.task_set_status_running(task['id'], task_ext_id, parser_id=self.id)
            return task_ext_id
        finally:
            await self.storage.update_task(task['id'],
                {'add_task_retries': task['add_task_retries'] + 1})
            success_callback(task['id'])

    async def check_task(self, task):
        state = await self.client.get_task_state(task["ext_id"])
        logger.debug("Task %s status: %s", task["id"], state["status"])
        # if state['status'] in (TaskStatus.COMPLETED, TaskStatus.ERROR):
        return state

    async def stop_task(self, task):
        await self.client.change_task_status(task["ext_id"], AParserTaskStatus.STOPPING)
        logger.debug("Stopping Task %s", task["id"])

    async def delete_task(self, task):
        await self.client.change_task_status(task["ext_id"], AParserTaskStatus.DELETING)
        logger.debug("Deleting Task %s", task["id"])

    async def get_result(self, task):
        result = await self.client.get_task_result(task["ext_id"])
        logger.debug("Task %s result ok", task["id"])
        return result

    async def get_task_state(self, task):
        return await self.client.get_task_state(task["ext_id"])

    async def get_task_result_file(self, task):
        return await self.client.get_task_result_file(task["ext_id"])

    async def delete_task(self, task):
        await self.client.change_task_status(task['ext_id'], AParserTaskStatus.DELETING)

    async def bulk_request(self, request):
        params = await registry.get_bulk_request_handler(request).make_parser_params()
        return await self.client.bulk_request(**params)

    async def get_info(self):
        return await self.client.info()

    async def get_tasks_list(self):
        return await self.client.get_tasks_list()

    def __repr__(self):
        return "<Parser {} [{}]>".format(self.url, self.free_slots)


class Manager:
    """
    Parser manager
    """
    PARSER_CHECK_TIMEOUT = 0.5

    def __init__(self, service):
        self.service = service
        self.period = 5
        self.parsers_registry = {}
        self.parsers_cycle = None

    async def run(self):
        parsers = await self.service.storage.get_parsers()
        for p in parsers:
            parser = Parser(p['id'], p['url'], p['password'], p['slots_number'], self.service.storage)
            self.parsers_registry[p['id']] = parser
        logger.info("Init with parsers %s", self.parsers_registry)
        self.parsers_cycle = itertools.cycle(self.parsers_registry.values())
        while True:
            logger.debug("manager tick")
            await asyncio.sleep(self.period)
            try:
                await self.loop()
            except Exception:
                logger.exception("Manager loop error")

    async def loop(self):
        logger.info("Parsers summary:")
        for p in self.parsers_registry.values():
            logger.info("Parser %s %s free slots", p.url, p.free_slots)

    async def get_idle_parser(self):
        coros = [p.check() for p in self.parsers_registry.values()]
        logger.info("checking parsers %s", coros)
        if not coros:
            return None
        done, pending = await asyncio.wait(coros, timeout=self.PARSER_CHECK_TIMEOUT)
        logger.info("checking parsers done")
        if pending:
            logger.info("pending info %s", pending)

        all_idle = [p for p in self.parsers_registry.values() if p.free_slots > 0]
        logger.info("Idle parsers: %s", all_idle)
        if all_idle:
            parser = sorted(all_idle, key=lambda p: p.free_slots, reverse=True)[0]
            logger.info("Yield: %s", parser)
            return parser

    async def check_task(self, task):
        parser = self.parsers_registry.get(task['parser_id'])
        if parser is None:
            logger.error("Unknown parser for task %s", task)
            return
        # TODO: set task status
        try:
            res = await parser.check_task(task)
            res['parser'] = parser.url
        except ApiServerError as e:
            logger.warn("Check Task error: %s", e)
            await self.service.storage.task_set_error(task['id'], {'check_task_error': e.message})
            return

        state = res["state"]
        if res['status'] == TaskStatus.ERROR:
            if task['on_error_retries'] > ON_ERROR_MAX_RETRIES:
                await self.service.storage.task_set_error(task['id'], res)
            else:
                await self.service.storage.task_retry_on_error(task)

        elif res['status'] == TaskStatus.COMPLETED:
            await self.service.storage.task_set_status_pending(task['id'])
        else:
            threshold = task["params"].get("complete_threshold")
            if threshold and state["queriesDoneCount"] > 0:
                # TODO should we stop/delete parser task??
                request_completed_percent = (state["queriesDoneCount"] / state["queriesCount"]) * 100
                if request_completed_percent >= threshold:
                    logger.info("Completed by threshold %s: %s", threshold, task['id'])
                    await self.service.storage.task_set_status_pending(task['id'])
                    await parser.stop_task(task)

    async def get_result(self, task):
        parser = self.parsers_registry.get(task['parser_id'])
        if parser is None:
            logger.error("Unknown parser for task %s", task)
            await self.service.storage.task_set_error(
                task['id'], {'get_result_error': "Missed parser #{}".format(task['parser_id'])})
            return
        # TODO: set task status
        try:
            parser_result = await parser.get_result(task)
            result = await registry.get_task_handler(task).process_parser_result(parser_result)
            await self.service.storage.task_set_result(task['id'], result, parser_result)
            await parser.delete_task(task)
        except Exception as e:
            logger.exception('Get Result errror for task %s', task['id'])
            if task['on_error_retries'] > ON_ERROR_MAX_RETRIES:
                await self.service.storage.task_set_error(task['id'], {'get_result_error': str(e)})
            else:
                await self.service.storage.task_retry_on_error(task)

    async def get_task_state(self, task):
        parser = self.parsers_registry.get(task['parser_id'])
        if parser is None:
            logger.debug("Unknown parser for task %s", task)
            res = {'state': task['status'], 'parser': None}
            if task['status'] == TaskStatus.ERROR:
                res['Error'] = task['parser_status']
            return res
        res = await parser.get_task_state(task)
        res['parser'] = parser.url
        return res

    async def get_task_result_file(self, task):
        parser = self.parsers_registry.get(task['parser_id'])
        if parser is None:
            logger.error("Unknown parser for task %s", task)
            return
        res = await parser.get_task_result_file(task)
        return res

    async def callback(self, task):
        try:
            async with aiohttp.ClientSession() as client:
                logger.info("Callback url: %s for task: %s", task['callback'], task['id'])
                async with client.post(task['callback'], json=task['result']) as resp:
                    logger.debug("Callback status: %s for task: %s", resp.status, task['id'])
        except Exception:
            logger.exception("Callback Error for task %s", task['id'])
            await self.service.storage.task_set_callback_done(task['id'], http_status=500)
        else:
            await self.service.storage.task_set_callback_done(task['id'], http_status=resp.status)

    async def bulk_request(self, request):
        parser = next(self.parsers_cycle)
        parser_result = await parser.bulk_request(request)
        result = await registry.get_bulk_request_handler(
            request).process_parser_result(parser_result)
        return result

    async def get_parser_info(self, parser_id):
        parser = self.parsers_registry.get(parser_id)
        if parser is None:
            return None
        else:
            return await parser.get_info()

    async def get_parser_tasks_list(self, parser_id):
        parser = self.parsers_registry.get(parser_id)
        if parser is None:
            return None
        else:
            return await parser.get_tasks_list()
