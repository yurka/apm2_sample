import logging

import configargparse
from dotenv import load_dotenv, find_dotenv

from .service import Service

load_dotenv(find_dotenv())


logger = logging.getLogger(__name__)


def run():
    p = configargparse.ArgParser(description='AParser Manager Service.',
                                 default_config_files=['/etc/apm-service.conf'])
    p.add('-c', '--config', required=False, is_config_file=True, help='config file path')
    p.add('--db-dsn', required=True, help='Database connection string', env_var='APM_DATABASE_DSN')
    p.add('--api-bind', required=False, help='API bind (host:port)',
          env_var='APM_API_BIND', default='127.0.0.1:8100')
    p.add('--log-level', help='log level', default='INFO')
    p.add('--authtoken', help='API Auth Token, use "NOAUTH" value to disable authentication',
          env_var='APM_API_AUTHTOKEN')

    options = p.parse_args()

    if options.db_dsn:
        print(options.db_dsn)

    logging.basicConfig(
        format='%(asctime)-15s %(levelname)-8s %(name)s: %(message)s',
        level=options.log_level
    )

    service = Service(options)
    service.run()


if __name__ == '__main__':
    run()
