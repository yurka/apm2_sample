from logging import getLogger

from impala.dbapi import connect


logger = getLogger(__name__)


class ImpalaService:
    """
    Interaction with Impala DB
    """
    _conn = None

    @property
    def conn(self):
        if self._conn is None:
            self._conn = connect(host='cloudera02.wiseweb.co', port=21050)
        return self._conn

    def get_keywords_weights(self, table_name, keywords, exclude):
        result = []
        cursor = self.conn.cursor(dictify=True)
        sql = """SELECT pr.keyword, pr.total_weight, pr.weight
        FROM {table} as pr
        INNER JOIN
            (SELECT keyword, max(updatedat) as max_updatedat
            FROM {table}
            GROUP BY keyword) as prw
        ON pr.keyword = prw.keyword
        AND pr.updatedat = prw.max_updatedat
        WHERE pr.total_weight IS NOT NULL AND {condition}
        """
        args = []
        if keywords:
            condition = 'pr.keyword RLIKE %s'
            args.append('|'.join(keywords))
        else:
            condition = ''

        if exclude:
            if condition:
                condition += ' AND pr.keyword NOT RLIKE %s'
            else:
                condition = 'pr.keyword NOT RLIKE %s'
            args.append('|'.join(exclude))

        sql = sql.format(table=table_name, condition=condition)
        logger.info('IMPALA QUERY: %s, args: %s', sql, args)
        cursor.execute(sql, args)
        for row in cursor.fetchall():
            result.append(row)
        return result

    def get_keywords_clusters(self, table_name, keywords, exclude, limit,
                              offset):
        result = []
        limit_construction = ''
        offset_construction = ''
        sql = """
        SELECT cl.n_cl3, cl.keyword, ws.total_weight,
            ws.weight, s.backlights FROM {table} as cl
        LEFT JOIN games_wordstat_40 as ws
        ON (cl.keyword=ws.keyword)
        JOIN (SELECT keyword, group_concat(backlights, ',') as backlights
            from aparser_upload_data_7762 GROUP BY keyword) as s
        ON cl.keyword = s.keyword
        WHERE cl.n_cl3 IN (SELECT n_cl3 FROM {table}
        {include_condition}
        {exclude_condition}
        )
        ORDER BY cl.n_cl3
        {limit_construction}
        {offset_construction}
        """

        args = []
        if keywords:
            include_condition = 'WHERE keyword RLIKE %s'
            args.append('|'.join(keywords))
        else:
            include_condition = ''

        if exclude:
            exclude_condition = 'AND keyword NOT RLIKE %s'
            args.append('|'.join(exclude))
        else:
            exclude_condition = ''

        if limit:
            limit_construction = 'LIMIT %s' % limit
        if offset:
            offset_construction = 'OFFSET %s' % offset
        sql = sql.format(table=table_name, include_condition=include_condition,
                         exclude_condition=exclude_condition,
                         limit_construction=limit_construction,
                         offset_construction=offset_construction)
        logger.info('IMPALA QUERY: %s, %s', sql, args)
        cursor = self.conn.cursor(dictify=True)
        cursor.execute(sql, args)

        for row in cursor.fetchall():
            row['backlights'] = ', '.join(
                set(row['backlights'].replace(', ', ',').split(',')) -
                set(row['keyword'].split(' ') + [row['keyword']])
            )
            result.append(row)
        return result

    def get_category_clusters(self, table_name, category, cluster_num):
        result = {}
        cursor = self.conn.cursor(dictify=True)
        sql = """
        select {table}.keyword,
        max({table}.url_count) as url_count,
        max({table}.n_cl3) as n_cl3,
        max({table}.n_cl4) as n_cl4,
        max({table}.n_cl5) as n_cl5,
        max(p.total_weight) as total_weight,
        max(p.weight) as weight,
        max(categories.category) as category
        from {table}
        join categories
        on {table}.id = categories.id
        join (
        SELECT pr.keyword, pr.total_weight, pr.weight, max_updatedat
        FROM parser_results_wordstat as pr
        INNER JOIN
        (SELECT keyword, max(updatedat) as max_updatedat
        FROM parser_results_wordstat
        GROUP BY keyword) as prw
        ON pr.keyword = prw.keyword
        AND pr.updatedat = prw.max_updatedat
        ) as p
        on {table}.keyword = p.keyword
        where p.total_weight IS NOT NULL
        group by {table}.keyword
        having category = %s
        """

        cluster_key = 'n_cl{}'.format(cluster_num)
        cursor.execute(sql.format(table=table_name), [category])
        for row in cursor.fetchall():
            cluster_id = row[cluster_key]
            item = {'keyword': row['keyword'],
                    'weight': row['weight'],
                    'total_weight': row['total_weight']}
            result.setdefault(cluster_id, {'keywords': []})['keywords'].append(item)
        for cluster_id, data in result.items():
            keywords = data['keywords']
            total_weight_cluster = sum([0 if kw['total_weight'] is None else kw['total_weight'] for kw in keywords])
            weight_cluster = sum([0 if kw['weight'] is None else kw['weight'] for kw in keywords])
            result[cluster_id]['total_weight_cluster'] = total_weight_cluster
            result[cluster_id]['weight_cluster'] = weight_cluster
        return result
