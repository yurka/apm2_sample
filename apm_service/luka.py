input_str = "applepie"

input_str2 = "appletopie"
dictionary = set(["apple", "pie", "app"])

result = "apple pie"


def segment(input_str):
    process_str = input_str
    result = []
    for w in dictionary:
        if process_str.startswith(w):
            result.append(w)
            process_str = process_str.replace(w, "", 1)
        if len(process_str) == 0:
            return " ".join(result)
    return None


def segment_2(input_str):
    process_str = input_str

    for n in range(1, len(process_str) + 1):
        w = process_str[:n]
        if w in dictionary:
            if n < len(process_str):
                subsegment = segment_2(process_str[n:])
                if subsegment:
                    return [w] + subsegment
            else:
                return [w]

print(segment_2(input_str))
print(segment_2(input_str2))
